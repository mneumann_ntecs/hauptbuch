format = ARGV[0]
count = (ARGV[1] || "1e3").to_f.to_i
start_ts = Time.new(2016)
end_ts = Time.new(2017)
duration = end_ts - start_ts
step = (duration / count).to_i

class Transaction < Struct.new(:date, :code, :description, :postings)
	def display_tackler
		s = ""
		s << self.date.strftime("%FT%TZ")
		s << " (#{self.code})" if self.code
		s << " '#{self.description}\n"
		self.postings.each do |posting|
			s << posting.display
		end
		s
	end

	def display_ledger
		s = ""
		s << self.date.strftime("%F")
		s << " (#{self.code})" if self.code
		s << " #{self.description}\n"
		self.postings.each do |posting|
			s << posting.display
		end
		s
	end
end

class Posting < Struct.new(:account, :amount)
	def display
		s = " #{self.account}"
		s << "  #{self.amount}" if self.amount
		s << "\n"
		s
	end
end

def generate_transactions(count, start_ts, step)
	for i in 1..count
		ts = start_ts + (i * step)
		assets_acc = "a:ay%04d:am%02d" % [ts.year, ts.month]
		expenses_acc = "e:ey%04d:em%02d:ed%02d" % [ts.year, ts.month, ts.day]
		yield Transaction.new(ts, "#%07d" % i, "txn-%d" % i,
				      [
					      Posting.new(expenses_acc, "%d.0000001" % ts.day),
					      Posting.new(assets_acc, nil)
				      ])
	end
end

case format
when 'tackler:txn'
	generate_transactions(count, start_ts, step) do |txn|
		puts txn.display_tackler
		puts
	end
when 'ledger'
	generate_transactions(count, start_ts, step) do |txn|
		puts txn.display_ledger
		puts
	end
when 'tackler:accounts'
	require 'set'
	coa = Set.new
	generate_transactions(count, start_ts, step) do |txn|
		txn.postings.each do |posting|
			coa << posting.account
		end
	end
	puts "accounts {"
	puts " strict = true"
	puts " coa = ["
	puts coa.to_a.sort.map {|i| "  " + i.inspect}.join(",\n")
	puts " ]"
	puts "}"
else
	raise
end
