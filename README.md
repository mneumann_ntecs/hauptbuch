# Hauptbuch - Plain-text accounting in Rust

A plain-text double-entry accounting system written in Rust

The format used for the journal is very similar to that used by
[Tackler][tackler] ([see journal format][tackler-journal]).

## Goals

* Fast
* Simple
* Correct

## Why yet another plain-text accounting system?

* I started to use Ledger, the original software, which is awesome but also
  very complex. I roughly have only a need for 1% of it's features. Nothing
  wrong with it except maybe that it's written in C++ ;-)

* Hledger sounded like a compelling alternative to Ledger but I were not able
  to compile it for my operating system (DragonFly BSD).

* I strongly believe in learning by doing. And as I am not an accountant, but want to
  get more into this topic, I started with this project.

## Performance

Hauptbuch is really fast and runs on low memory! Single file performance of
balancing 1E6 (one million) transactions is ~1.5 seconds (3 MB memory usage,
single core). [Benchmarks of Tackler][tackler-performance] on the other hand
report for the same dataset ~30 seconds and peak memory usage of around 6-7
GB!!! My own tests indicate that Hauptbuch is ~200 times faster (on
single-core) and ~700 times more memory efficient. With 16 GB of RAM, Tackler
just failed to balance the 1E6 transactions.

For balancing, given that all accounts and commodities are known in advance,
Hauptbuch does not allocate memory at all throughout the whole balacing
process.

[tackler]: https://gitlab.com/e257/accounting/tackler
[tackler-journal]: https://tackler.e257.fi/docs/journal/format/
[tackler-performance]: https://tackler.e257.fi/docs/performance/ 
