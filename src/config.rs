use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Debug, Serialize, Deserialize)]
pub struct HauptbuchConfig {
    pub accounts: AccountsConfig,
    pub commodities: CommoditiesConfig,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct AccountsConfig {
    pub list: HashMap<String, AccountConfig>,
    pub strict: AccountsStrictConfig,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct AccountConfig {
    pub commodity: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct AccountsStrictConfig {
    pub enabled: bool,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct CommoditiesConfig {
    pub list: HashMap<String, CommodityConfig>,
    pub default: Option<String>,
    pub strict: CommoditiesStrictConfig,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct CommoditiesStrictConfig {
    pub enabled: bool,
    pub default_rounding: Option<u8>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct CommodityConfig {
    pub rounding: Option<u8>,
}

use hauptbuch_core::{AccountRepository, CommodityRepository};
use std::{error::Error, path::Path};

pub struct Config {
    pub commodities: CommodityRepository,
    pub accounts: AccountRepository,
}

fn parse_config_from_file<P: AsRef<Path>>(path: P) -> Result<HauptbuchConfig, Box<dyn Error>> {
    use std::{fs::File, io::BufReader};
    let file = File::open(path)?;
    let reader = BufReader::new(file);
    let config = serde_json::from_reader(reader)?;
    Ok(config)
}

pub fn default_config() -> Config {
    use hauptbuch_core::{AccountStrictMode, CommodityStrictMode, Rounding};
    Config {
        commodities: CommodityRepository::new(
            vec![],
            CommodityStrictMode::Disabled {
                default_rounding: Rounding::None,
            },
            None,
        )
        .unwrap(),
        accounts: AccountRepository::new(vec![], AccountStrictMode::Disabled).unwrap(),
    }
}

pub fn load_config_from_file<P: AsRef<Path>>(path: P) -> Result<Config, Box<dyn Error>> {
    let config = parse_config_from_file(path)?;
    let mut commodities = config_to_commodities(&config.commodities).unwrap();
    let accounts = config_to_accounts(&config.accounts, &mut commodities).unwrap();
    Ok(Config {
        commodities,
        accounts,
    })
}

fn config_to_commodities(config: &CommoditiesConfig) -> Result<CommodityRepository, ()> {
    use hauptbuch_core::{Commodity, CommodityStrictMode, Rounding};

    fn convert_rounding(rounding: Option<u8>) -> Rounding {
        match rounding {
            None => Rounding::None,
            Some(n) => Rounding::BankersDp(n),
        }
    }

    let commodity_list: Vec<Commodity> = config
        .list
        .iter()
        .map(|(k, v)| Commodity::new(k.into(), convert_rounding(v.rounding)))
        .collect();

    let strict_mode = if config.strict.enabled {
        CommodityStrictMode::Enabled
    } else {
        CommodityStrictMode::Disabled {
            default_rounding: convert_rounding(config.strict.default_rounding),
        }
    };
    let default_commodity = config.default.as_ref().map(|s| s.as_str());

    CommodityRepository::new(commodity_list, strict_mode, default_commodity)
}

fn config_to_accounts(
    config: &AccountsConfig,
    commodities: &mut CommodityRepository,
) -> Result<AccountRepository, ()> {
    use hauptbuch_core::{Account, AccountStrictMode, OptValue, Value};

    let strict_mode = if config.strict.enabled {
        AccountStrictMode::Enabled
    } else {
        AccountStrictMode::Disabled
    };

    let accounts_list: Result<Vec<Account>, ()> = config
        .list
        .iter()
        .map(|(k, v)| {
            Ok(Account::new_with_value(
                k.into(),
                match &v.commodity {
                    Some(commodity) => {
                        let commodity_id = commodities.get_or_add(&commodity)?;
                        let value = Value::zero(commodity_id);
                        OptValue::new_with_value(value)
                    }
                    None => OptValue::new(),
                },
            ))
        })
        .collect();

    AccountRepository::new(accounts_list?, strict_mode)
}
