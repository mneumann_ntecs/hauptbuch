use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(
    name = "hauptbuch",
    about = "Plain-text double-entry accounting system"
)]
pub struct Cli {
    #[structopt(short = "c", long = "config", parse(from_os_str))]
    pub config_file: Option<PathBuf>,

    #[structopt(subcommand)]
    pub cmd: Command,
}

#[derive(Debug, StructOpt)]
pub enum Command {
    #[structopt(name = "list-accounts")]
    ListAccounts(ListAccountsCommand),
    #[structopt(name = "balance")]
    Balance(BalanceCommand),
    #[structopt(name = "register")]
    Register(RegisterCommand),
}

#[derive(Debug, StructOpt)]
pub struct ListAccountsCommand {
    #[structopt(parse(from_os_str))]
    pub inputs: Vec<PathBuf>,
}

#[derive(Debug, StructOpt)]
pub struct BalanceCommand {
    #[structopt(parse(from_os_str))]
    pub inputs: Vec<PathBuf>,
}

#[derive(Debug, StructOpt)]
pub struct RegisterCommand {
    // pub account: String,
    #[structopt(parse(from_os_str))]
    pub inputs: Vec<PathBuf>,
}
