pub(crate) mod cli;
pub(crate) mod commands {
    pub mod balance;
    pub mod list_accounts;
    pub mod register;
}

pub(crate) mod config;

use cli::{Cli, Command};
use structopt::StructOpt;

fn main() {
    let cli = Cli::from_args();
    let config = if let Some(path) = cli.config_file {
        self::config::load_config_from_file(path).unwrap()
    } else {
        self::config::default_config()
    };

    match cli.cmd {
        Command::ListAccounts(ref args) => {
            commands::list_accounts::list_accounts(args);
        }
        Command::Balance(ref args) => {
            commands::balance::command::balance(config, args);
        }
        Command::Register(ref args) => {
            commands::register::command::register(config, args);
        }
    }
}
