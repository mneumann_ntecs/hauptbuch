use crate::cli::ListAccountsCommand;
use hauptbuch_core::parser_models::*;
use hauptbuch_core::{LineProcessor, LineVisitor};
use std::collections::HashSet;

struct PostingVisitor {
    used_accounts: HashSet<String>,
}

impl LineVisitor for PostingVisitor {
    type Error = ();
    fn visit_posting<'a>(&mut self, posting: Posting<'a>) -> Result<(), ()> {
        self.used_accounts.insert(posting.account_name.to_string());
        Ok(())
    }
}

pub fn list_accounts(args: &ListAccountsCommand) {
    let mut posting_visitor = PostingVisitor {
        used_accounts: HashSet::new(),
    };

    for input_file in &args.inputs {
        let _processed_lines_count = LineProcessor
            .run_file(input_file, &mut posting_visitor)
            .unwrap();
    }

    report(&posting_visitor.used_accounts);
}

fn report(used_accounts: &HashSet<String>) {
    let mut accounts: Vec<_> = used_accounts.iter().collect();
    &mut accounts[..].sort_by(|a, b| a.to_lowercase().cmp(&b.to_lowercase()));

    for account in &accounts {
        println!("{}", account);
    }
}
