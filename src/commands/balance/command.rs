use super::report::balance_report;
use crate::cli::BalanceCommand;
use crate::config;
use hauptbuch_core::{
    transaction_processor::TransactionCommitter, Balance, DummyTransactionFilter, LineProcessor,
    TransactionProcessor,
};

pub fn balance(config: config::Config, args: &BalanceCommand) {
    let config::Config {
        accounts,
        commodities,
    } = config;

    let mut balance_processor: TransactionProcessor<_, _, ()> = TransactionProcessor::new(
        Balance {
            accounts,
            commodities,
        },
        Box::new(DummyTransactionFilter),
        Box::new(TransactionCommitter),
        (),
    );
    for input_file in &args.inputs {
        let _processed_lines = LineProcessor
            .run_file(input_file, &mut balance_processor)
            .unwrap();
    }

    let mut report = balance_report(balance_processor.get_balance());
    report.sort_by_currency();
    report.display();
}
