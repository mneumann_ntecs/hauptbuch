use hauptbuch_core::{
    decimal::{Decimal, Zero},
    Balance, MultiValue, OptValue,
};
use std::collections::BTreeMap;

pub fn balance_report(data: Balance) -> BalanceReport {
    struct AccountNode {
        tree_sum: MultiValue,
        balance: OptValue,
    }

    let mut total_sum = MultiValue::new();
    let mut account_nodes = BTreeMap::<&str, AccountNode>::new();

    for account in data.accounts.account_iter() {
        let name = &account.fullname;

        // For each parent account...
        for (pos, _) in name.match_indices(':') {
            let parent = &name[..pos];
            let parent_node = account_nodes.entry(parent).or_insert(AccountNode {
                tree_sum: MultiValue::new(),
                balance: OptValue::new(),
            });
            parent_node.tree_sum.add_opt_value(&account.value);
        }
        // This is the actual account of name `name`
        let account_node = account_nodes.entry(name).or_insert(AccountNode {
            tree_sum: MultiValue::new(),
            balance: OptValue::new(),
        });
        assert!(account_node.balance.value.is_none());
        account_node.balance = account.value;
        account_node.tree_sum.add_opt_value(&account.value);
        total_sum.add_opt_value(&account.value);
    }

    let mut account_balances = Vec::new();

    for (&acct_name, node) in account_nodes.iter() {
        // The commodity of node.balance is contained in tree_sum.
        for tree_sum_value in node.tree_sum.values() {
            let commodity_id = tree_sum_value.commodity_id;
            account_balances.push(AccountBalance {
                account_name: acct_name.into(),
                account_sum: node
                    .balance
                    .amount_in_commodity(commodity_id)
                    .unwrap_or(Zero::zero()),
                account_tree_sum: tree_sum_value.amount,
                commodity: data
                    .commodities
                    .lookup_name_by_id(commodity_id)
                    .unwrap()
                    .into(),
            });
        }
    }

    let mut deltas = vec![];

    for total_sum_value in total_sum.values() {
        deltas.push(DeltaBalance {
            amount: total_sum_value.amount,
            commodity: data
                .commodities
                .lookup_name_by_id(total_sum_value.commodity_id)
                .unwrap()
                .into(),
        });
    }

    BalanceReport {
        balances: account_balances,
        deltas,
    }
}

struct AccountBalance {
    account_name: String,
    account_sum: Decimal,
    account_tree_sum: Decimal,
    commodity: String,
}

struct DeltaBalance {
    amount: Decimal,
    commodity: String,
}

pub struct BalanceReport {
    balances: Vec<AccountBalance>,
    deltas: Vec<DeltaBalance>,
}

impl BalanceReport {
    pub fn sort_by_currency(&mut self) {
        self.balances[..].sort_by(|a, b| a.commodity.cmp(&b.commodity));
        self.deltas[..].sort_by(|a, b| a.commodity.cmp(&b.commodity));
    }

    pub fn display(&self) {
        println!("BALANCE");
        println!("-------");
        let max_commodity_len = self
            .balances
            .iter()
            .map(|acct| acct.commodity.len())
            .max()
            .unwrap_or(0);

        for balance in self.balances.iter() {
            println!(
                "  {amt:>19.2} {tree_amt:>19.2} {commodity:<width$}  {acct:}",
                width = max_commodity_len,
                amt = balance.account_sum,
                tree_amt = balance.account_tree_sum,
                commodity = balance.commodity,
                acct = balance.account_name
            );
        }
        println!("==============================");
        for delta in self.deltas.iter() {
            println!(
                "  {amt:>19.2} {commodity:<width$}",
                width = max_commodity_len,
                amt = delta.amount,
                commodity = delta.commodity
            );
        }
    }
}
