use super::report::RegisterReport;
use crate::cli::RegisterCommand;
use crate::config;
use hauptbuch_core::{
    parser_models, transaction_processor::AdditionalTransactionInfo, Balance,
    DummyTransactionFilter, LineProcessor, TransactionProcessor, TransactionRecorder,
};

pub fn register(config: config::Config, args: &RegisterCommand) {
    let config::Config {
        accounts,
        commodities,
    } = config;

    let mut transaction_builder: TransactionProcessor<_, _, AdditionalTransactionInfo> =
        TransactionProcessor::new(
            Balance {
                accounts,
                commodities,
            },
            Box::new(DummyTransactionFilter),
            Box::new(TransactionRecorder::new()),
            AdditionalTransactionInfo {
                local_timezone: parser_models::Timezone::Utc,
            },
        );

    for input_file in &args.inputs {
        let _processed_lines = LineProcessor
            .run_file(input_file, &mut transaction_builder)
            .unwrap();
    }

    let (accounts, commodities, recorder) = transaction_builder.unwrap();

    let mut transactions = recorder.transactions;

    (&mut transactions[..]).sort_by(|a, b| a.info.datetime.cmp(&b.info.datetime));

    let report = RegisterReport {
        accounts,
        commodities,
        transactions,
    };
    report.display();
}
