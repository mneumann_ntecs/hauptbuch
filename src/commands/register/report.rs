use hauptbuch_core::{
    chrono::{Datelike, Timelike},
    parser_models,
    transaction_processor::{AdditionalTransactionInfo, ResolvedTransactionRecord},
    AccountRepository, CommodityRepository,
};

pub struct RegisterReport {
    pub accounts: AccountRepository,
    pub commodities: CommodityRepository,
    pub transactions: Vec<ResolvedTransactionRecord<AdditionalTransactionInfo>>,
}

fn display_datetime(datetime: &parser_models::DateTime) {
    match datetime {
        parser_models::DateTime::Date(d) => {
            print!("{:04}-{:02}-{:02}", d.year(), d.month(), d.day());
        }
        parser_models::DateTime::DateTime(dt) => {
            print!(
                "{:04}-{:02}-{:02}T{:02}:{:02}:{:02}",
                dt.year(),
                dt.month(),
                dt.day(),
                dt.hour(),
                dt.minute(),
                dt.second()
            );
        }
        parser_models::DateTime::DateTimeTz(dt, tz) => {
            print!(
                "{:04}-{:02}-{:02}T{:02}:{:02}:{:02}",
                dt.year(),
                dt.month(),
                dt.day(),
                dt.hour(),
                dt.minute(),
                dt.second()
            );
            match tz {
                parser_models::Timezone::Utc => print!("Z"),
                parser_models::Timezone::FixedOffset(off) => print!("{}", off),
            }
        }
    }
}

fn display_transaction(
    accounts: &mut AccountRepository,
    commodities: &CommodityRepository,
    transaction: &ResolvedTransactionRecord<AdditionalTransactionInfo>,
) {
    display_datetime(&transaction.info.display_datetime);

    let mut max_width = 0;

    let header = if let Some(code) = &transaction.info.code {
        format!(" ({}) '{}", code, transaction.info.description)
    } else {
        format!(" '{}", transaction.info.description)
    };
    max_width = max_width.max(header.len());
    println!("{}", header);

    for posting in transaction.postings.iter() {
        let account = accounts.get_by_id_mut(posting.account_id).unwrap();
        let value = posting.value.as_ref().unwrap().value;
        let commodity = commodities.lookup_name_by_id(value.commodity_id).unwrap();

        account.transfer(value).unwrap();

        let new_account_value = account.value.value.unwrap();

        let line = format!(
            "            {acct_name:<19}{transfer_amt:>19.2}{new_account_value:>19.2} {commodity}",
            acct_name = account.fullname,
            transfer_amt = value.amount,
            new_account_value = new_account_value.amount,
            commodity = commodity
        );

        max_width = max_width.max(line.len());
        println!("{}", line);
    }
    println!("{:-<1$}", "", max_width);
}

impl RegisterReport {
    pub fn display(self) {
        let RegisterReport {
            mut accounts,
            commodities,
            transactions,
        } = self;
        let _ = commodities;

        println!("REGISTER");
        println!("--------");

        for txn in transactions.iter() {
            display_transaction(&mut accounts, &commodities, &txn);
        }
    }
}
